package = "mpv-luasockets"
version = "0.1-1"

source = {
   url = "https://codeberg.org/tplasdio/mpv-luasockets"
}

description = {
   detailed = "Control `mpv` running instances from your shell via mpv's JSON IPC, for Unix-like platforms.",
   homepage = "https://codeberg.org/tplasdio/mpv-luasockets",
   license = "GPLv3 or later"
}

dependencies = {
   "lua >= 5.1",
   "luaposix >= 35.0-1",
   "argparse >= 0.7.1-1",
   "dkjson >= 2.6-1",
}

build = {
    type = "builtin",
    modules = {
        ["mpvs"] = "src/mpvs.lua",
    },
    install = {
        bin = {
            "src/bin/mpvs",
            "src/bin/mpvsk"
        }
    }
}

