export PATH := lua_modules/bin:$(PATH)
SRC = src
BIN = $(SRC)/bin
VERSION=$(shell awk '/VERSION/{print $$4; exit}' $(BIN)/mpvs)
REVISION=1
ROCKSPEC=mpv-luasockets-$(VERSION)-$(REVISION).rockspec

init: hooks install

all:
	luarocks --local make $(ROCKSPEC)

hooks:
	git config core.hooksPath .githooks

install:
	luarocks --tree=lua_modules install --only-deps mpv-luasockets-0.1-1.rockspec
