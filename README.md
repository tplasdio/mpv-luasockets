# Mpv-luasockets

Control `mpv` running instances from your shell via mpv's JSON IPC, for Unix-like platforms.

When launching `mpv`, one can use `--ipc-socket` (or set the property in `mpv.conf`) to spawn _one_ Unix socket for controlling it, but whenever a new instance is launched, the old socket gets disconnected.

To solve this, the `mpvsk` wrapper script creates a unique IPC socket for each `mpv` instance, spawned at `/run/user/uid/mpvsockets/` by default, or at a directory in the `$MPV_SOCKET_DIR` environment variable. I alias it in my shell like this: `MPV='mpvsk'; alias mpv='"$MPV"'`, so every time I launch mpv I use the wrapper instead.

Now, the `mpvs` script can send commands to the running instances

## Help
```
Usage: mpvs ([--playing] | [--paused]) [-h] [--version] [--interactive]
     <command> ...

Control mpv instances via Unix sockets

Options:
   -h, --help                 Show this help message and exit.
   --version                  Show version
   --playing, -p              Filter to only playing sockets
   --paused, +p               Filter to only paused sockets
   --interactive, -i          Pick sockets interactively with fzf

Commands:
   list, ls                   List active sockets
   command, cmd, c            Send command/JSON to sockets
   play-pause, pp             Toggle playing/pausing sockets
   play                       Play paused sockets
   pause                      Pause playing sockets
   next, n, →                 Next file in playlist
   previous, prev, ←          Previous file in playlist
   quit, q                    Quit mpv instance
   quit-watch-later, qwl      Quit and save location
   mute, m                    Toggle mute
   shuffle, shuf              Toggle shuffling of playlist
   loop, l                    Toggle infinite/no loop
   speed, sp                  Set the speed
   keypress, key, k           Press a key to a socket
   property, prop             Property operations
   seek, s                    Fast forward/backwards
   volume, vol, v             Set the volume
   pitch, pi                  Set the pitch
```

## Examples
```sh
mpvsk "file1.mkv" "file2.flac" &   # Start mpv with socket
mpvsk "file3.webm" "file4.mp3" &   # Another one
mpvs ls          # List running active sockets
mpvs pause       # Pause instances
mpvs m           # Mute instances
mpvs play        # Resume playing instances
mpvs m           # Unmute instances
mpvs sp +.1      # Increase speed of instances
mpvs pitch 1.2   # Set pitch of instances
mpvs n           # Play next file in playlist of instances
mpvs s 1:00      # Fast forward 1 minute in instances
mpvs s -- -30    # Fast backwards 30 seconds in instances
mpvs prop -g metadata  # Get metadata of instances
mpvs c 'command' # Send any other mpv command normally in mpv's input.conf
mpvs q           # Quit instances
```

## Dependencies
- `mpv`
- `lua`
- `lua-posix`
- `lua-dkjson`
- `lua-argparse`
- `fzf` (Optional for the -i flag)

Tested with Lua 5.4 and 5.3

## Installation

Install `luarocks`, then run `luarocks --local make *.rockspec`, or
through the makefile: `make all`

## Similar

- [`mpvc`](https://gitlab.com/mpv-ipc/mpvc)
- [`mpv-sockets`](https://github.com/seanbreckenridge/mpv-sockets)
- [`python-mpv`](https://github.com/gustaebel/python-mpv)

## License
GPLv3 or later
