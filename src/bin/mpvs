#!/usr/bin/lua5.3

-- This file is part of mpv-luasockets.
-- mpv-luasockets is free software: you can redistribute it and/or modify it under
-- the terms of the GNU General Public License as published by the Free Software
-- Foundation, either version 3 of the License, or (at your option) any later version.
--
-- mpv-luasockets is distributed in the hope that it will be useful, but WITHOUT
-- ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
-- FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License along with
-- mpv-luasockets. If not, see <https://www.gnu.org/licenses/>.

---------------
-- ## mpvs, control mpv running instances from your shell.
--
-- @author Tsplasdio at codeberg dot org
-- @copyright 2022
-- @license GPLv3 or later
-- @script mpvs
--
-- As mpv states:
-- This is not intended to be a secure network protocol. It is explicitly insecure.
-- For example, the run command is exposed, which can run arbitrary system commands.
-- The use-case is controlling the player locally.

local Mpvs = require("mpvs")
local argparse = require("argparse")
local dkjson = require("dkjson")

local VERSION = 0.1

--- Invoke command and retrive output
-- @tparam string shell command
-- @treturn string command standard output
-- @usage
-- os.sh("echo hello")  -- "hello"
function os.sh(command)
	local fp = io.popen(command)
	if fp == nil then
		return nil
	end
	local stdout = fp:read('*a')
	fp:close()
	return stdout
end


--- Command-line argument parsing
-- @tparam string table of command line arguments
-- @usage
-- args = parse_cmdargs(arg)
local function parse_cmdargs(cmdargs)

	local parser = argparse() -- for the help option 🆘
		:name "mpvs"
		:description "Control mpv instances via Unix sockets"
		:help_max_width(80)
		:usage_max_width(80)
		:usage_margin(5)
		:help_description_margin(30)
		:add_complete()

	parser:flag("--version")
		:description("Show version")
		:action(function() print(VERSION); os.exit(0) end)

	-- Filtering options
	parser:mutex(
		parser:flag("--playing -p")
			:description("Filter to only playing sockets")
		,
		parser:flag("--paused +p")
			:description("Filter to only paused sockets")
	)

	parser:flag("--interactive -i")
		:description("Pick sockets interactively with fzf")
		-- TODO: also options for rofi and dmenu

	-- TODO:
	--	- if no argument passed, list all sockets
	--	- add option '-' for reading socket on which to operate from stdin
	--	- more consistent naming of subarguments

	parser:command("list ls") --🆔
		:description("List active sockets")

	local subparser_command = parser:command("command cmd c")
		:description("Send command/JSON to sockets")

	-- Should this be a global option that only acts on certain subcommands?
	subparser_command:flag("-r")
		:target('command_r')
		:description("Wait for response from socket, and show it")
		:default(false)

	subparser_command:argument("json")
		:target('json_command')

	-- Should these be subcommands? they would be mutex, but
	-- one may want to use more than one at a time
	parser:command("play-pause pp") --⏯️ ⏸️ ▶️
		:description("Toggle playing/pausing sockets")
	parser:command("play") -- ▶️
		:description("Play paused sockets")
	parser:command("pause") -- ⏸️
		:description("Pause playing sockets")
	parser:command("next n →") --⏭️
		:description("Next file in playlist")
	parser:command("previous prev ←") --⏮️
		:description("Previous file in playlist")
	parser:command("quit q") --❎
		:description("Quit mpv instance")
	parser:command("quit-watch-later qwl")
		:description("Quit and save location")
	--parser:command("stop")
		--:description("Clear playlist keeping socket alive")
	parser:command("mute m") --🔇
		:description("Toggle mute")
	parser:command("shuffle shuf") --🔀
		:description("Toggle shuffling of playlist")
	-- parser:command("reverse rev ◀️")
	 parser:command("loop l") --🔁
		:description("Toggle infinite/no loop")
	-- parser:command("loop-once")  --🔂
	-- parser:command("dim")  --🔅
	-- parser:command("bright")  --🔆
	-- parser:command("record") --⏺️ for stop recording ⏹️
	-- -- for restarting song: 🔄
	-- -- for loadfile 📁

	local subparser_speed = parser:command("speed sp") -- ⏫ ⏬
		:description("Set the speed")

	subparser_speed:argument("speed")
		:target("speed_value")
		:description([[
		Speed by which to increase, decrease (if starts with + or -) or set (if it doesn't)
		]])

	local subparser_keypress = parser:command("keypress key k")
		:description("Press a key to a socket")

	subparser_keypress:argument("key")
		:description("Key to press")
	-- TODO:
	-- Add suboption to specify how many times to press it
	-- Allow to press multiple keys at a time

	local subparser_property = parser:command("property prop")
		:description("Property operations")

	subparser_property:option("--get -g")
		:description("Get the values of properties")
		:args("*")

	subparser_property:option("--set -s")
		:description("Set the values of properties")
		:args("*")

	local subparser_seek = parser:command("seek s") -- ⏩ ⏪
		:description("Fast forward/backwards")

	subparser_seek:argument("seconds")
		:target("seconds_seek")
		:default("+5")
		:description([[
		Time in seconds to seek, negative numbers must be prefixed with --
		]])

	local subparser_volume = parser:command("volume vol v") -- 🔼 🔽
		:description("Set the volume")

	subparser_volume:argument("volume_change")
		:target("vol_change")
		:default("+5")
		:description([[
		Units of volume to increase, decrease (if starts with + or -) or set (if it doesn't)
		]])

	local subparser_pitch = parser:command("pitch pi")
		:description("Set the pitch")

	subparser_pitch:argument("pitch_value")
		:default("1")
		:description([[
		Pitch to change to (1 is normal pitch)
		]])

	return parser:parse(cmdargs)
end


local function main()
	local args = parse_cmdargs(arg)

	local active_sockets, errmsg, errcode = Mpvs:get_active_sockets()
	local selected_sockets
	-- TODO: wrap some of these in methods

	if active_sockets then
		--Filtering {{{
		-- These should be methods to 'active_sockets'
		if args["playing"] ~= nil or
			args["paused"] ~= nil
		then
			local rs = Mpvs:send_command(
				'{"command":["get_property","pause"]}',
				active_sockets,
				true
			)
			local paused_sockets, playing_sockets = {}, {}
			for socket,res in pairs(rs) do
				if dkjson.decode(res).data then
					table.insert(paused_sockets, socket)
				else
					table.insert(playing_sockets, socket)
				end
			end
			if args["playing"] ~= nil then
				selected_sockets = playing_sockets
			else
				selected_sockets = paused_sockets
			end
		else
			selected_sockets = active_sockets
		end

		if args["interactive"] then
			-- TODO:
			--	- add preview in fzf with information of the socket
			--	- check if fzf is actually installed

			-- Ideally entries should be read separated by null bytes with '--read0', "\0", but I couldn't make it work
			local fzf_output = os.sh([[fzf -m -i --print0 --prompt='Select mpv sockets: ' <<-\EOF
			]]..table.concat(selected_sockets, "\n"))

			selected_sockets = {}
			for _ in fzf_output:gmatch("([^\0]+)") do
				table.insert(selected_sockets, _)
			end
		end
		--}}}

		if args["list"] ~= nil then
			if #selected_sockets == 0 then
				io.stderr:write("No sockets matching filtering selection\n")
				os.exit(3)
			end
			for _,socket in ipairs(selected_sockets) do
				print(socket)
			end
		end

		if args["command"] ~= nil then
			Mpvs:send_command(args.json_command, selected_sockets, args["command_r"])
		end

		if args["property"] ~= nil then

			local json_property_cmd = {
				command = {}
			}

			if args["get"] then
				-- Here there should be an option to wheter print the JSON back to the
				-- shell or parse it and show it in another format

				-- Also, it makes more sense to group the returns by socket, so maybe
				-- I should store them and at the end print it well, or refactor the Mpvs:send_command function
				for _,property in ipairs(args["get"]) do
					json_property_cmd.command = {
						"get_property", property
					}
					local rs = Mpvs:send_command(
						dkjson.encode(json_property_cmd),
						selected_sockets,
						true
					)
					-- TODO:
					-- add functions for pretty formatting, specially certain properties
					-- that would benefit like 'filename'
					for socket,res in pairs(rs) do io.write(socket.."\t"..res) end
				end
			end

			if args["set"] then
				for _,property_kv in ipairs(args["set"]) do
					-- Properties and values should be provided as property=value on the shell
					-- This ↑ should be validated with a function in the parser with the :convert() method
					-- that also trims whitespace
					local property, value = property_kv:match("(.-)=(.*)")
					-- bug: some values are not parsed well to a JSON because they are strings, like "true" instead of true
					-- since mpv's IPC also accepts = as key-value separator, maybe I could pass it directly as string?
					json_property_cmd.command = {
						"set_property", property, value
					}
					Mpvs:send_command(
						dkjson.encode(json_property_cmd),
						selected_sockets
					)
				end
			end
		end

		-- TODO: Optimize this ↓ with a table that maps this script subcommands to mpv commands

		-- Sometimes there's 2 responses, maybe that's the bug with broken pipes ↑
		if args["play-pause"] ~= nil then
			Mpvs:send_command('cycle pause', selected_sockets)
		end

		if args["play"] ~= nil then
			-- emits "unpause" event on response
			Mpvs:send_command('set pause no', selected_sockets)
		end

		if args["pause"] ~= nil then
			-- emits "pause" event on response
			Mpvs:send_command('set pause yes', selected_sockets)
		end

		if args["next"] ~= nil then
			-- emits "audio-reconfig","tracks-changed" events on response
			Mpvs:send_command('playlist-next', selected_sockets)
		end

		if args["previous"] ~= nil then
			-- emits "audio-reconfig","tracks-changed" events on response
			Mpvs:send_command('playlist-prev', selected_sockets)
		end

		if args["quit"] ~= nil then
			-- emits "audio-reconfig" event on response
			Mpvs:send_command('quit', selected_sockets)
			-- Maybe killing the PID (which is the name of the socket) is more robust
			for _,socket in ipairs(selected_sockets) do
				os.remove(socket)
			end
		end

		-- It doesn't keep the socket alive unlike what the mpv manual says?
		--if args["stop"] ~= nil then
			--Mpvs:send_command('stop', selected_sockets)
		--end

		if args["quit-watch-later"] ~= nil then
			Mpvs:send_command('quit-watch-later', selected_sockets)
			for _,socket in ipairs(selected_sockets) do
				os.remove(socket)
			end
		end

		if args["mute"] ~= nil then
			Mpvs:send_command('cycle mute', selected_sockets)
		end

		if args["seek"] ~= nil then
			-- emits "seek" event on response
			Mpvs:send_command('seek '..args["seconds_seek"], selected_sockets)
		end

		if args["volume"] ~= nil then
			local vol_change_op = args["vol_change"]:sub(1,1)
			if  vol_change_op == '+' or
				vol_change_op == '-'
			then
				Mpvs:send_command('add volume '..args["vol_change"], selected_sockets)
			elseif vol_change_op == '*' then
				Mpvs:send_command('multiply speed '..args["vol_change"]:sub(2), selected_sockets)
			else
				Mpvs:send_command('set volume '..args["vol_change"], selected_sockets)
			end
		end

		if args["shuffle"] ~= nil then
			-- TODO: toggle playlist-{shuffle,unshuffle}
			Mpvs:send_command('playlist-shuffle', selected_sockets)
		end

		if args["loop"] ~= nil then
			Mpvs:send_command('cycle-values loop-file "inf" "no"', selected_sockets)
		end

		if args["speed"] ~= nil then
			local speed_value_op = args["speed_value"]:sub(1,1)
			if  speed_value_op == '+' or
				speed_value_op == '-'
			then
				Mpvs:send_command('add speed '..args["speed_value"], selected_sockets)
			elseif speed_value_op == '*' then
				Mpvs:send_command('multiply speed '..args["speed_value"]:sub(2), selected_sockets)
			else
				Mpvs:send_command('set speed '..args["speed_value"], selected_sockets)
			end
		end

		if args["pitch"] ~= nil then
			-- emits "audio-reconfig" event on response
			Mpvs:send_command(
				'af set rubberband=pitch-scale='..args["pitch_value"],
				selected_sockets
			)
		end

		if args["keypress"] ~= nil then
			Mpvs:send_command('keypress '..args["key"], selected_sockets)
		end

	else
		io.stderr:write(errmsg.."\n")
		os.exit(errcode)
	end
end

if not pcall(debug.getlocal, 4, 1) then
	main()
end
