#!/usr/bin/lua5.3

-- This file is part of mpv-luasockets.

-- mpv-luasockets is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

-- mpv-luasockets is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

-- You should have received a copy of the GNU General Public License along with mpv-luasockets. If not, see <https://www.gnu.org/licenses/>. 

local posix = require("posix")
local socket = require("posix.sys.socket")
local sys_stat = require("posix.sys.stat")
local json = require("dkjson")

local VERSION = 0.1

-- As mpv states:
-- This is not intended to be a secure network protocol. It is explicitly insecure.
-- For example, the run command is exposed, which can run arbitrary system commands.
-- The use-case is controlling the player locally.

local Mpvs = {
	socket_dir = os.getenv("MPV_SOCKET_DIR") or (os.getenv("XDG_RUNTIME_DIR") or "/run/user/"..posix.unistd.getuid()).."/mpvsockets"
}

function Mpvs._only_sockets(files)
	--- Return only files that are sockets
	local socket_files = {}
	for _,file in ipairs(files) do
		local info = assert(sys_stat.stat(file)) -- Will this fail if not enough permissions?
		if sys_stat.S_ISSOCK(info.st_mode) == 1 then
			table.insert(socket_files, file)
		end
	end
	return socket_files
end


function Mpvs.start_socket(socket_file)
	--- Start socket, check if we can connect to a socket file
	local fd = socket.socket (socket.AF_UNIX, socket.SOCK_STREAM, 0)

	local ok,errmsg,errcode = socket.connect(fd, {
		family = socket.AF_UNIX,
		path = socket_file
	})

	-- Returns connection values
	return {
		file = socket_file,
		fd =fd,
		ok = ok,
		errmsg = errmsg,
		errcode = errcode
	}
end


function Mpvs:start_socket_callback(socket_file, callback)
	--- Start socket and do something on successful connection
	return self:_handle_socket_connect(self.start_socket(socket_file), callback)
end


function Mpvs:_handle_socket_connect(connection, callback)
	--- What to do when socket connection is successful or not
	local cb_return
	if connection.errcode then
		-- TODO: show this only ↓ if an option --verbose is passed
		io.stderr:write("ERROR: "..connection.errmsg..", for file "..connection.file..", removing...\n")
		os.remove(connection.file)
	else
		cb_return = callback(connection)
		-- Close connection
		local ok, errmsg = socket.shutdown(connection.fd, socket.SHUT_RDWR)
	end
	return cb_return
end


function Mpvs:get_active_sockets()
	--- Get active sockets in the socket dir
	local active_sockets = {}
	local retmsg,retcode

	local files,errcode = posix.glob(self.socket_dir.."/*")

	if files then
		local socket_files = self._only_sockets(files)

		for _,file in ipairs(socket_files) do
			-- Only get the sockets with successful connections
			self:start_socket_callback(file, function()
				table.insert(active_sockets, file)
			end)
		end

		if #active_sockets == 0 then
			retmsg,retcode = "No active sockets found", 2
			return nil, retmsg, retcode
		else
			return active_sockets
		end

	else
		retmsg,retcode = "No files found in "..self.socket_dir, 1
		return nil, retmsg, retcode
	end
end


-- TODO: have a method and cmdline option to run Mpvs as a daemon
-- for the sending to be faster, also once it's daemonized, have an option
-- to run mpv as child processes of the daemon, setting sockets appropiately

function Mpvs:send_command(json_cmd, sockets_paths, respond)
	--- Send a JSON command (passed as table or string) to sockets

	if type(json) == table then
		local json_cmd = json.encode(json_cmd)
	end
	local respond = respond or false
	local responses = {}

	for _,socket_path in ipairs(sockets_paths) do

		local response = self:start_socket_callback(socket_path, function(con)
			socket.send(con.fd, json_cmd.."\n")
			-- It should error() out if this fails

			-- Only get response if true provided as argument
			if respond then
				local response = socket.recv(con.fd, 1000000)
				-- TODO: Add a timeout
				return response
			end
		end)
		responses[socket_path] = response
	end
	return responses
end

return Mpvs
